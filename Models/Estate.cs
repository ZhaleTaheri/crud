﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AsaNii.Models
{
    public class Estate
    {
        [Key]
        public int Id { get; set; }

        [DisplayName("شماره")]
        [Required(ErrorMessage = "لطفا شماره ملک  مورد نظر وارد نمایید!")]
        public int Number { get; set; }

        [Required(ErrorMessage = "لطفا نام ملک  مورد نظر وارد نمایید!")]
        [StringLength(256)]
        [DisplayName("نام")]

        public String Name { get; set; }

        [DisplayName("متراژ")]
        [Required(ErrorMessage = "لطفا متراژ ملک را وارد نمایید!")]
        public decimal Area { get; set; }

        [Required(ErrorMessage = "لطفا آدرس را وارد نمایید!")]
        [StringLength(1000)]
        [DisplayName("آدرس")]
        public string Address { get; set; }

        [Required]
        [DisplayName("جنوبی")]
        public bool IsSouthern { get; set; }

        public bool IsDeleted { get; set; }

        [ForeignKey("LandlordId")]
        [DisplayName("مالک")]
        public virtual Landlord Landlord { get; set; }

        [DisplayName("مالک")]
        public int LandlordId { get; set; }
    }
}
