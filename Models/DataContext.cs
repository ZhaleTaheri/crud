﻿using Microsoft.EntityFrameworkCore;

namespace AsaNii.Models
{
    public class DataContext : DbContext
    {
        public DataContext()
        {
        }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
        }
        public DbSet<Estate> Estates { get; set; }
        public DbSet<Landlord> Landlord { get; set; }
    }
}
