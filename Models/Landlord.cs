﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AsaNii.Models
{
    public class Landlord
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(256)]
        public String FirstName { get; set; }

        [Required]
        [StringLength(256)]
        public String LastName { get; set; }

        [NotMapped]
        public String FullName { get => FirstName + " " + LastName; }

        [Required]
        [StringLength(20)]
        public string Phone { get; set; }

    }
}
