﻿using AsaNii.Infrastructure.Repositories.Interfaces;
using AsaNii.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AsaNii.Infrastructure.Repositories
{

    public class LandlordRepository : ILandlordRepository
    {
        private readonly DataContext _context;
        public LandlordRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<List<Landlord>> Get()
        {
            return await _context.Landlord.ToListAsync();
        }

        public async Task<Landlord> Get(int id)
        {
            return await _context.Landlord.FindAsync(id);
        }

    }
}
