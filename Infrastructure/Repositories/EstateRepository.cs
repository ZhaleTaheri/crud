﻿using AsaNii.Infrastructure.Repositories.Interfaces;
using AsaNii.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AsaNii.Infrastructure.Repositories
{

    public class EstateRepository : IEstateRepository
    {
        private readonly DataContext _context;
        public EstateRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<List<Estate>> Get()
        {
            var dBContext = _context.Estates.Include(e => e.Landlord);
            return await dBContext.ToListAsync();
        }

        public async Task Create(Estate estate)
        {
            _context.Add(estate);
            await _context.SaveChangesAsync();
        }

        public async Task<Estate> Get(int id)
        {
            return await _context.Estates.FindAsync(id);
        }

        public async Task Update(Estate estate)
        {
            _context.Update(estate);
            await _context.SaveChangesAsync();
        }

        public async Task Delete(Estate estate)
        {
            _context.Estates.Remove(estate);
            await _context.SaveChangesAsync();
        }
    }
}
