﻿using AsaNii.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AsaNii.Infrastructure.Repositories.Interfaces
{
    public interface IEstateRepository
    {
        Task<List<Estate>> Get();
        Task Create(Estate estate);
        Task<Estate> Get(int id);
        Task Update(Estate estate);
        Task Delete(Estate estate);
    }
}
