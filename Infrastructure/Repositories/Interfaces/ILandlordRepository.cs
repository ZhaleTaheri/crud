﻿using AsaNii.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AsaNii.Infrastructure.Repositories.Interfaces
{
    public interface ILandlordRepository
    {
        Task<List<Landlord>> Get();
        Task<Landlord> Get(int id);
    }
}
