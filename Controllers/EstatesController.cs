﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AsaNii.Models;
using AsaNii.Infrastructure.Repositories.Interfaces;

namespace AsaNii.Controllers
{
    public class EstatesController : Controller
    {
        private readonly IEstateRepository _estateRepository;
        private readonly ILandlordRepository _landlordRepository;

        public EstatesController(IEstateRepository estateRepository, ILandlordRepository landlordRepository)
        {
            _estateRepository = estateRepository;
            _landlordRepository = landlordRepository;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            return View(await _estateRepository.Get());
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            var landlords = await _landlordRepository.Get();
            ViewData["LandlordId"] = new SelectList(landlords, "Id", "FullName");
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Estate estate)
        {
            if (ModelState.IsValid)
            {
                await _estateRepository.Create(estate);
                return RedirectToAction(nameof(Index));
            }

            var landlords = await _landlordRepository.Get();
            ViewData["LandlordId"] = new SelectList(landlords, "Id", "FullName", estate.LandlordId);
            return View(estate);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                var landlords = await _landlordRepository.Get();
                ViewData["LandlordId"] = new SelectList(landlords, "Id", "FullName");
                return View();
            }
            else
            {
                var estate = await _estateRepository.Get(id.Value);
                if (estate == null)
                {
                    return NotFound();
                }

                var landlords = await _landlordRepository.Get();
                ViewData["LandlordId"] = new SelectList(landlords, "Id", "FullName", estate.LandlordId);
                return View(estate);
            }

        }

        [HttpPost]
        public async Task<IActionResult> Edit(int id, Estate estate)
        {
            if (id != estate.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _estateRepository.Update(estate);
                }
                catch (DbUpdateConcurrencyException)
                {
                    var estateExist = await EstateExists(estate.Id);

                    if (!estateExist)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            var landlords = await _landlordRepository.Get();
            ViewData["LandlordId"] = new SelectList(landlords, "Id", "FullName", estate.LandlordId);
            return View(estate);
        }

        [HttpDelete]
        public async Task Delete(int id)
        {
            var estate = await _estateRepository.Get(id);
            await _estateRepository.Delete(estate);
        }

        private async Task<bool> EstateExists(int id)
        {
            return await _estateRepository.Get(id) != null;
        }
    }
}
